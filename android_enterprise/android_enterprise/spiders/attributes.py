import scrapy
import json


class AttributesSpider(scrapy.Spider):
    name = 'attributes'
    allowed_domains = ['https://androidenterprisepartners.withgoogle.com/']
    start_urls = ['https://androidenterprisepartners.withgoogle.com/_ah/spi/search/v1/devices?size=300&sort=createDate:desc,sort_name:asc']

    def parse_phone(self, item):
        yield

    def parse(self, response):
        data = json.loads(response.text)
        for item in data['items']:
            yield{
                'model': item['displayName'],
                'os': item['os_version_facet'],
                'sensors': item['hardwareFeatures']['fingerPrintSupport'],
                'ioxt': item['certifications_obj']['ioxt'],
                'ioxtUrl': item['certifications_obj']['ioxtUrl'],
                'commmonCriteria': item['certifications_obj']['commonCriteria'],
                'commmonCriteriaUrl': item['certifications_obj']['commonCriteriaUrl']
                }
        
