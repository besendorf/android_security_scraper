import scrapy
from scraper_api import ScraperAPIClient
from urllib.parse import urljoin

class AttributeSpider(scrapy.Spider):
    client = ScraperAPIClient('xxx')
    name = 'attributes'
    start_urls = [client.scrapyGet(url = 'https://www.gsmarena.com/makers.php3')]
#    allowed_domains = ['www.gsmarena.com']
        
    def parse_attributes(self, response):
        if response.css('td.nfo[data-spec=os]::text').get() is not None:
            if 'Android' in response.css('td.nfo[data-spec=os]::text').get():
                fingerprint = False
                if response.css('td.nfo[data-spec=sensors]::text').get() is not None:
                    if 'Fingerprint' in  response.css('td.nfo[data-spec=sensors]::text').get():
                        fingerprint = True
                yield {
                    'model': response.css('h1::text').get(),
                    'os': response.css('td.nfo[data-spec=os]::text').get(),
                    'chipset': response.css('td.nfo[data-spec=chipset]::text').get(),
                    'fingerprint': fingerprint 
                    }
    
    def parse_brand(self, response):
        phone_links = response.css('li a::attr(href)')
        for link in phone_links:
            link = urljoin('https://www.gsmarena.com', link.get())
            if '-' in link: #all phone link have a '-' in the url, but other sites like news.php don't
                yield scrapy.Request(self.client.scrapyGet(url = link), callback=self.parse_attributes)
    
    def parse(self, response):
        brand_links = response.css('td a::attr(href)')
        for link in brand_links:
                link = urljoin('https://www.gsmarena.com', link.get())
                if 'phones' in link:
                    yield scrapy.Request(self.client.scrapyGet(url = link), callback=self.parse_brand)