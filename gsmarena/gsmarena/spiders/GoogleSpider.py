import scrapy

class AttributeSpider(scrapy.Spider):
    name = 'google'
    start_urls = ['https://webcache.googleusercontent.com/search?q=cache:u8gj9AaWq-MJ:https://www.gsmarena.com/makers.php3']
    # allowed_domains = ['https://www.gsmarena.com']
        
    def parse_attributes(self, response):
        yield {
            'model': response.css('h1::text').get(),
            'os': response.css('td.nfo[data-spec=os]::text').get(),
            'chipset': response.css('td.nfo[data-spec=chipset]::text').get(),
            'sensors': response.css('td.nfo[data-spec=sensors]::text').get() #fingerprint
            }
    
    def parse_brand(self, response):
        phone_links = response.css('li a')
        yield from response.follow_all(phone_links, self.parse_attributes)
    
    def parse(self, response):
        brand_links = response.css('td a')
        yield from response.follow_all(brand_links, self.parse_brand)