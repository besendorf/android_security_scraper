import urllib.request
import urllib.parse
import csv
import os
import sys

def download(url):
    if url != '':
        print(url)
        print(url.split('/')[-1])
        try:
            urllib.request.urlretrieve(iri2uri(url.replace(':443','')), url.split('/')[-1])
        except ConnectionResetError:
            print('Connection reset by server. Continuing')
        except urllib.request.HTTPError:
            print('File Not Found. Continuing')
        except Exception as e:
            print('Unexpected error. Continuing')
            print(e)

        
def iri2uri(url):
    #converts IRIs to URIs as urllib only supports ASCII
    url = list(urllib.parse.urlsplit(url))
    url[2] = urllib.parse.quote(url[2])
    return urllib.parse.urlunsplit(url)

csv_url = 'https://www.commoncriteriaportal.org/products/certified_products.csv'
# download the csv from commoncriterialportal.org
urllib.request.urlretrieve(csv_url, 'certified_products.csv')
# filter Mobility devices
with open('certified_products.csv', 'r', errors="ignore") as certified_products, open('filtered.csv', 'w', newline='') as filtered:
    writer = csv.writer(filtered)
    reader = csv.reader(certified_products)
    line = 0
    for line, row in enumerate(reader):
        if 'Mobility' or 'ICs, Smart Cards and Smart Card-Related Devices and Systems' in row or line == 0:
            writer.writerow(row)
            
try:
    os.mkdir('pdf')
except FileExistsError:
    pass
except OSError:
    print("Creation of pdf directory failed. exiting")
    sys.exit(1)


with open ('filtered.csv', 'r') as filtered:
    try:
        os.chdir('pdf')
    except OSError:
        print("Changing directory failed. exiting")
        sys.exit(1)
    csv_dict = csv.DictReader(filtered)
    for row in csv_dict:
        download(row['Certification Report URL'])
        download(row['Security Target URL'])
        download(row['Maintenance Report'])
        download(row['Maintenance ST'])
        

